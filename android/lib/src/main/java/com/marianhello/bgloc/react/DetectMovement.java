package com.marianhello.bgloc.react;

/**
 * Created by Theepika on 6/22/18.
 */

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Theepika on 3/20/18.
 * This class listens to the sensor listener and sends sos alert to server.
 * Listen to accelorometer and sends data to server
 */

public class DetectMovement extends Service implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private long mShakeTimestamp;
    private long lastUpdatedTime;
    private RequestQueue mRequestQueue;
    private static final float SHAKE_THRESHOLD_GRAVITY = 3.4F;
    private static final String BASE_URL = "https://tracker.tictoctrack.com/api/UserMobileApp";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //Return the current date and time in the UTC format.
    public static String getDateTime(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone((TimeZone.getTimeZone("UTC")));
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    //This function will be called when user force quits the applcation, we can find this process and stop motion detection service
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        SharedPreferences sharedPrefs = getSharedPreferences("DATA", MODE_PRIVATE);
        sharedPrefs.edit().putBoolean("ISCLOSED", true).apply();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("serviceStarted", "serviceStarted");
        SharedPreferences sharedPrefs = getSharedPreferences("DATA", MODE_PRIVATE);

        mSensorManager = (SensorManager) getSystemService(getApplicationContext().SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);

        return super.onStartCommand(intent, flags, startId);
    }

    //Function returns the request queue, If queue hasn't been created yet, create one. Else use the same instance
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(DetectMovement.this);
        }
        return mRequestQueue;
    }

    //Function to find battery level in percnetage works only if version is more than LOLLIPOP
    private int findBatterLevel() {
        BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
        int batLevel = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
            Log.d("battery level", String.valueOf(batLevel));
        } else {
            batLevel = 0;
        }
        return batLevel;
    }

    //Function sends SOS alert to the server
    public void sendSOSAlert() {
        long now = System.currentTimeMillis();
        if (now >= (lastUpdatedTime + 30 * 1000)) {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }

            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("DATA", MODE_PRIVATE);


            final Location location = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            final TimeZone tz = TimeZone.getDefault();

            final String token = sharedPreferences.getString("TOKEN", null);

            mRequestQueue = getRequestQueue();
            StringRequest postRequest = new StringRequest(Request.Method.POST, BASE_URL+"/ReportLocation",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Succesfully sent sos ", response);
                            Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            v.vibrate(1000);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("error occured in sos", String.valueOf(error));
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("error occured in sos", String.valueOf(error));
                            VolleyLog.d("Error", "Error: " + error.getMessage());
                        }
                    };
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("latitude", String.valueOf(location.getLatitude()));
                    params.put("longitude", String.valueOf(location.getLongitude()));
                    params.put("speed", String.valueOf(location.getSpeed()));
                    params.put("isLocated", "true");
                    params.put("isSOS", "true");
                    params.put("deviceTime", getDateTime());
                    params.put("timeZone", String.valueOf(tz.getID()));
                    params.put("batteryLevel", String.valueOf(findBatterLevel()));

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Authorization", "Token " + token);
                    return params;
                }
            };
            Volley.newRequestQueue(DetectMovement.this).add(postRequest);
            lastUpdatedTime = now;
        }

    }

    //On service destroy stop detecting the sensor listeners of the device.
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSensorManager != null) {
            Log.d("onDestroy", mSensorManager.toString());
            mSensorManager.unregisterListener(this, mAccelerometer);
            mSensorManager = null;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        float gX = x / SensorManager.GRAVITY_EARTH;
        float gY = y / SensorManager.GRAVITY_EARTH;
        float gZ = z / SensorManager.GRAVITY_EARTH;

        // gForce will be close to 1 when there is no movement.
        float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);
        long now = event.timestamp;

        //Check if the force value is more than the default threshold, if so emit this event, else skip
        if (gForce >= SHAKE_THRESHOLD_GRAVITY) {
            if (now >= (mShakeTimestamp + 30 * 1000)) {
                Log.d("gForcefromservice", String.valueOf(gForce));
                sendSOSAlert();
                mShakeTimestamp = now;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}