package com.marianhello.bgloc;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class HttpPostService {
    private static final String BASE_URL = "https://tracker.tictoctrack.com/api/UserMobileApp";


    public static int postJSON(String url, Object json, Map headers) throws IOException {
        String jsonString = json.toString();
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setDoOutput(true);
        conn.setFixedLengthStreamingMode(jsonString.length());
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        Iterator<Map.Entry<String, String>> it = headers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            conn.setRequestProperty(pair.getKey(), pair.getValue());
        }

        OutputStreamWriter os = null;
        try {
            os = new OutputStreamWriter(conn.getOutputStream());
            os.write(json.toString());
        }catch (Exception e){
            Log.d("ERRORMESSAGE SENDDATA", String.valueOf(e));

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        Log.d("ERRORMESSAGE",conn.getResponseMessage() + json.toString() + url + headers.toString());
        return conn.getResponseCode();
    }

    public static String resetToken(String token) throws IOException{
        String newToken = null;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("oldtoken", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HttpURLConnection conn = (HttpURLConnection) new URL(BASE_URL + "/RenewToken").openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");


        OutputStreamWriter os = null;
        try {
            os = new OutputStreamWriter(conn.getOutputStream());
            os.write(jsonObject.toString());
        }catch (Exception e){
            Log.d("ERRORMESSAGE SENDDATA", String.valueOf(e));

        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }

        String result = readInputStreamToString(conn);
        try {
            JSONObject obj = new JSONObject(result);
            JSONObject userAppToken = obj.getJSONObject("userapptoken");
            newToken = userAppToken.getString("authToken");

            Log.d("ERRORMESSGAENEW",newToken + userAppToken);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("ERRORMESSAGETOKEN",conn.getResponseMessage() + result);

        return newToken;

    }

    private static String readInputStreamToString(HttpURLConnection connection) {
        String result = null;
        StringBuffer sb = new StringBuffer();
        InputStream is = null;

        try {
            is = new BufferedInputStream(connection.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
        }
        catch (Exception e) {
            Log.d("ERRORMESSAGE", "Error reading InputStream");
            result = null;
        }
        finally {
            if (is != null) {
                try {
                    is.close();
                }
                catch (IOException e) {
                    Log.d("ERRORMESSAGE", "Error closing InputStream");
                }
            }
        }

        Log.d("ERRORMESSAGERESULT", result);
        return result;
    }


    public static int postFile(String url, File file, Map headers, UploadingCallback callback) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

        conn.setDoInput(false);
        conn.setDoOutput(true);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            conn.setFixedLengthStreamingMode(file.length());
//        } else {
//            conn.setChunkedStreamingMode(0);
//        }
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json");
        Iterator<Map.Entry<String, String>> it = headers.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pair = it.next();
            conn.setRequestProperty(pair.getKey(), pair.getValue());
        }


        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
            Log.d("FILEUPLOAD error",String.valueOf(e));
        }


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userlocationparamlist", text);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        OutputStreamWriter os = null;
        try {
            os = new OutputStreamWriter(conn.getOutputStream());
            os.write(jsonObject.toString());
        }catch (Exception e){
            Log.d("ERRORMESSAGEEXCEP", String.valueOf(e));
        } finally {
            if (os != null) {
                os.flush();
                os.close();
            }
        }
        Log.d("ERRORMESSAGEFROMFILE",conn.getResponseMessage() + text.toString() + url + headers.toString());
        return conn.getResponseCode();



    }
}
